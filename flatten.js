// function flatten(elements,depth) {
//   if (Array.isArray(elements) === false) {
//     return [];
//   }

//   let newArr = [];

//   function checkArray(elements) {
//     for (let i = 0; i < elements.length; i++) {
//       if (Array.isArray(elements[i]) === true) {
//         checkArray(elements[i]);
//       } else {
//         newArr.push(elements[i]);
//       }
//     }
//   }

//   checkArray(elements);

//   return newArr;
// }
function flatten(element, depth = Infinity) {
  let finalArr = true;

  let input;
  while (depth > 0 && finalArr) {
    let newArr = [];
    for (let i = 0; i < element.length; i++) {
      input = element[i];
      if (Array.isArray(input) === true && depth > 0) {
        newArr.push(...input);
      } else if (element[i] === undefined) {
        continue;
      } else {
        newArr.push(input);
      }
      for (let j = 0; j < newArr.length; j++) {
        let total = newArr[j];
        finalArr = Array.isArray(total);
        if (finalArr) {
          break;
        } else {
          continue;
        }
      }
    }
    element = newArr;
    depth--;
  }
  return element;
}
module.exports = flatten;
