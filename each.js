function each(element, cb) {
  if (Array.isArray(element) === false) {
    return [];
  }

  for (let i = 0; i < element.length; i++) {
    element[i] = cb(element[i], i, element);
  }
  return element;
}
module.exports = each;
