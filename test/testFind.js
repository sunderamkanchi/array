let items = [1, 2, 3, 4, 5, 5];

let find = require("../find");

let actual = find(items, (data, index, arr) => {
  return data === 3;
});

console.log(actual);
