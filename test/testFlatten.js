let items = [1, [2], [[3]], [[[4]]]];

let flatten = require("../flatten");

let result = flatten(items);

console.log(result);
