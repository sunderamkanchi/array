let items = [1, 2, 3, 4, 5, 5];

let filter = require("../filter");

let manualResult = filter(items, (data, index, arr) => data > 3);
let idealResult = items.filter((data, index, arr) => data > 3);

if (JSON.stringify(manualResult) === JSON.stringify(idealResult)) {
  console.log(manualResult);
} else {
  console.log([]);
}
