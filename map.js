function map(element, cb) {
  if (cb === undefined) {
    return [];
  }
  if (Array.isArray(element) === false) {
    return [];
  }
  let newArray = [];
  for (let i = 0; i < element.length; i++) {
    let result = cb(element[i], i, element);
    newArray.push(result);
  }
  return newArray;
}

module.exports = map;
