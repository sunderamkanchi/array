function reduce(element, cb, firstValue) {
  if (firstValue === undefined) {
    firstValue = element[0];
  } else {
    firstValue = cb(firstValue, element[0]);
  }
  if (Array.isArray(element) === false) {
    return [];
  }
  for (let i = 1; i < element.length; i++) {
    firstValue = cb(firstValue, element[i]);
  }
  return firstValue;
}

module.exports = reduce;
