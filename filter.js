function filter(element, cb) {
  if (Array.isArray(element) === false) {
    return [];
  }
  let newArray = [];
  for (let i = 0; i < element.length; i++) {
    let result = cb(element[i], i, element);
    if (result === true) {
      newArray.push(element[i]);
    }
  }
  return newArray;
}

module.exports = filter;
